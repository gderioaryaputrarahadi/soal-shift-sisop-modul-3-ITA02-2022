#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <wait.h>

int main() {

  pid_t child_id = fork();
  if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", "/home/hanif/shift3/hartakarun", NULL};
    execv("/bin/mkdir", argv);
  }
  while(wait(NULL) != child_id);
  
  child_id = fork();
  if (child_id == 0) {
    char *argv[] = {"unzip", "-q", "hartakarun.zip", "-d", "/home/hanif/shift3", NULL};
    execv("/usr/bin/unzip", argv);
  }
  while(wait(NULL) != child_id);
}