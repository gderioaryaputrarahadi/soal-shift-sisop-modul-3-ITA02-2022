# soal-shift-sisop-modul-3-ITA02-2022
Anggota Kelompok:

-Asima Prima Yohana Tampubolon 5027201009

-Gde Rio Aryaputra Rahadi 5027201063

-Muhammad Hanif Fatihurrizqi 5027201068

# Soal 1

# 1.a 
Pada soal ini kita diminta untuk melakukan download file dan mengekstrak file dengan bantuan thread agar proses yang dilakukan terjadi secara bersamaan. 

# Jawab
Pada bagian ini, saya menggunakan library yang dibutuhkan pada program ini. Library yang saya 
gunakan adalah:

![](img/1.1.png)

Lalu, untuk mendowload file, pertama sekali saya mengecek apakah file yang ingin di download sudah ada di directory atau belum. Oleh sebab itu, saya melakukan deklarasi fungsi exists 
seperti berikut:

![](img/1.2.png)

Fungsi akan mengembalikan nilai 0 apabila belum ada dan mengembalikan 1 apabila sudah ada. 
Pada program, apabila return 0 maka akan mendownload file dengan menggunakan fungsi runCommand
Pada fungi runCommand, kita menggunakan algoritma spawning dengan menggunakan fork. 

![](img/1.3.png)

Lalu, file akan otomatis terdownload. Setelah itu, kita menggunakan while sebanyak 2 karena 
thread yang akan kita gunakan ada sebanyak 2. Lalu, kita menggunakan pthread_create untuk memulai thread. 

![](img/1.4.png)

Fungsi ini akan memanggil fungsi unzip yang sudah di deklarasikan terlebih dahulu. 

![](img/1.5.png)

Pada fungsi unzip ini, kita akan menggunakan pthread_equal untuk memasukkan file yang tepat di unzip menggunakan fungsi yang tepat. Lalu, file akan terunzip seperti gambar berikut

![](img/1.6.png)

# Error yang dialami
Tidak ada, namun hanya kesulitan dalam memahami algoritmanya sehingga membutuhkan waktu yang lama untuk menyelesaikannya.

# Soal 2

Soal ini meminta praktikan untuk membuat sistem online judge sederhana dengan implementasi client-server memanfaatkan socket programming

# Setup Server dan Client

Setup server:

```
int main(int argc, char const *argv[])
{
 int server_fd, new_socket, valread;
 struct sockaddr_in address;
 int opt = 1;
 int addrlen = sizeof(address);
 char buffer[1024] = {0};
 
 if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
 {
   perror("socket failed");
   exit(EXIT_FAILURE);
 }
 
 if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
 {
   perror("setsockopt");
   exit(EXIT_FAILURE);
 }
 
 address.sin_family = AF_INET;
 address.sin_addr.s_addr = INADDR_ANY;
 address.sin_port = htons(PORT);
 
 if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
 {
   perror("bind failed");
   exit(EXIT_FAILURE);
 }
 
 if (listen(server_fd, 3) < 0)
 {
   perror("listen");
   exit(EXIT_FAILURE);
 }
 
 if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
 {
   perror("accept");
   exit(EXIT_FAILURE);
 }
```
Setup client:

```
int main(int argc, char const *argv[])
{
 struct sockaddr_in address;
 int sock = 0, valread;
 struct sockaddr_in serv_addr;
 char buffer[1024] = {0};
 if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
 {
   printf("\n Socket creation error \n");
   return -1;
 }
 memset(&serv_addr, '0', sizeof(serv_addr));
 serv_addr.sin_family = AF_INET;
 serv_addr.sin_port = htons(PORT);
 
 if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
 {
   printf("\nInvalid address/ Address not supported \n");
   return -1;
 }
 if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
 {
   printf("\nConnection Failed \n");
   return -1;
 }
```
# Hasil Run

![](img/2.1.png)

![](img/2.2.png)

Server dan client berhasil hidup dan terhubung

# Soal 3

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

# Soal 3a

Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

# Penyelesaian

![](img/3.1.JPG)

1. Parent process akan membuat child process dimana dia akan membuat direktori pada /home/hanif/shift3/hartakarun menggunakan perintah mkdir. Disini argument -p digunakan agar directory yang belum dibuat (seperti /home/hanif/shift3) langsung terbuat.

2. Lalu parent process akan menunggu mkdir selesai dibuat. Lalu parent process akan membuat child process lagi untuk mengekstrak zip hartakarun.zip. Lalu parent process akan menunggu child process selesai.
