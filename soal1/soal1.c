#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/wait.h>
#include <wait.h>
#include <errno.h>

pthread_t tid[2];
pid_t child;

void *unzip(void *arg)
{
    pthread_t id = pthread_self();

    char *unzip_music[] = {"unzip",
                           "music.zip",
                           "-d",
                           "./music", NULL};
    char *unzip_quote[] = {"unzip",
                           "quote.zip",
                           "-d",
                           "./quote", NULL};

    if (pthread_equal(id, tid[0]))
    {
        if ((child = fork()) == 0)
        {
            execv("/usr/bin/unzip", unzip_music);
        }
    }
    else if (pthread_equal(id, tid[1]))
    {
        if ((child = fork()) == 0)
        {
            execv("/usr/bin/unzip", unzip_quote);
        }
    }
    return NULL;
}

int exists(const char *fname)
{
    DIR *dir = opendir(fname);
    if (dir)
    {
        closedir(dir);
        return 1;
    }
    else if (ENOENT == errno)
    {
        return 0;
    }
}

void runCommand(char command[], char *lines[])
{
    int status;
    pid_t child_id = fork();
    if (child_id == 0)
        execv(command, lines);
    else
    while (wait(&status) > 0);
}

int main(void)
{
    int i = 0;
    int err;

    char *music_link = "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1";
    char *quote_link = "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt";

    // Download database
    if ((exists("music") == 0) && (exists("quote") == 0))
    {
        char *getMusic[] = {"wget", music_link, "-O", "music.zip", "-o", "/dev/null", NULL};
        runCommand("/usr/bin/wget", getMusic);
        char *getQuote[] = {"wget", quote_link, "-O", "quote.zip", "-o", "/dev/null", NULL};
        runCommand("/usr/bin/wget", getQuote);
    }

    // Extact File
    while (i < 2)
    {
        error = pthread_create(&(tid[i]), NULL, unzip, NULL);
        if (error != 0)
        {
            printf("\n tidak dapat membuat thread\n");
        }
        else
        {
            printf("\n thread berhasil\n");
        }
        i++;
    }

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    exit(0);
    return 0;
}
